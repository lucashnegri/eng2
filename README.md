# Leia-me

Este é o repositório que será utilizado na disciplina de engenharia de software 2.

# git clone

Utilizado para copiar um repositório GIT por completo (tanto os arquivos do projeto quanto arquivos utilizados internamente pelo GIT).

No nosso caso, o comando é:

git clone https://lucashnegri@bitbucket.org/lucashnegri/eng2.git
